import {Component, Input, OnChanges, OnInit, Output, SimpleChange} from '@angular/core';
import {Category} from '../Data/category';
import {CategoryBase} from '../category-base';
import {DeleteCategoryDto} from '../Data/delete-category-dto';
import {Router} from '@angular/router';
import {CategoriesComponent} from '../categories/categories.component';
import {ParameterService} from '../parameter.service';
import {AdminService} from '../admin.service';

const SELECTED_CLASS = "bg-primary";
const UNSELECTED_CLASS = "bg-secondary";

@Component({
  selector: 'app-category-element',
  templateUrl: './category-element.component.html',
  styleUrls: ['./category-element.component.css']
})
export class CategoryElementComponent implements OnInit, OnChanges, CategoryBase {



  public icon = 'keyboard_arrow_right';
  public class = SELECTED_CLASS;
  isChecked: boolean = false;

  private _children: Category[];
  get children(): Category[]{
    return this._children;
  }
  @Input()
  set children(children: Category[]){
    this._children = children;
  }

  @Input() @Output()
  leftMargin: number = 0;

  private _category: Category;
  @Input()
  set category(category: Category){
    this._category = category;
  }
  get category(): Category{
    return this._category;
  }



  @Input()
  expanded: boolean = false;

  constructor(protected adminService: AdminService,
              private router: Router) {}

  ngOnInit() {}

  toggleExpanded(){
    this.expanded = !this.expanded;
    this.icon = this.expanded ? 'keyboard_arrow_down' : 'keyboard_arrow_right'
  }

  toggleSelected(){
    if(this.adminService.element === this) {
      return;
    }
    this.adminService.element = this;
  }



  getCategory(): Category {
    return this.category;
  }
  setCategory(category: Category) {
    this.category = category;
  }

  removeCategory(){
    const deleteCategoryDto = new DeleteCategoryDto();
    deleteCategoryDto.categoryId = this.category.idCategory;
    deleteCategoryDto.withChildren = this.isChecked;
    this.adminService.deleteCategory(deleteCategoryDto).subscribe( response => {
      if (response.ok) {
        this.router.navigate(['categoriesManagement']);
      }
    });
  }

  OnChange($event){
   // console.log($event);
    this.isChecked = $event.isChecked;
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    if(this.adminService.element === this) {
      this.class = SELECTED_CLASS;
    } else {
      this.class = UNSELECTED_CLASS;
    }
  }
}
