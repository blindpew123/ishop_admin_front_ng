import {Injectable} from '@angular/core';
import {JsIshopService} from './js-ishop-service';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {MatBottomSheet} from '@angular/material';
import {Observable} from 'rxjs';
import {Category} from './Data/category';
import {catchError, tap} from 'rxjs/operators';
import {Parameter} from './Data/parameter';
import {ParameterType} from './Data/parameter-type';
import {ShopItemSaveDto} from './Data/shop-item-save-dto';

@Injectable({
  providedIn: 'root'
})
export class ParameterService extends JsIshopService {

 /* private _paramters: Parameter[];
  get parameters(): Parameter[] {
    return this._paramters;
  }
  set parameters(parameters: Parameter[]) {
    this._paramters = parameters;
  }*/



  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  // оставить здесь
  getParametersForCategory(category: Category): Observable<Parameter[]> {
    return this.http.get<Parameter[]>(this.jsIshopUrl + `/getParametersForCategory/${category.idCategory}`)
      .pipe(tap(parameters => {
          console.log(parameters);
          this.handleSuccess('Список параметров загружен');
       // this.parameters = parameters;
        }),
        catchError(this.handleError(
          `Ошибка загузки списка параметров для категории ${category.idCategory}`, [])));
  }

  // оставить здесь
  getParameterTypes(): Observable<ParameterType[]> {
    return this.http.get<ParameterType[]>(this.jsIshopUrl + '/getParameterTypes')
      .pipe(tap(parameterTypes => {
          console.log(parameterTypes);
          this.handleSuccess('Список типов параметров загружен');
      //    this.parameterTypes = parameterTypes;
        }),
        catchError(this.handleError(
          'Ошибка загузки типов параметров', [])));
  }

  getParameterValuesForCategory(): Observable<ParameterType[]> {
    return this.http.get<ParameterType[]>(this.jsIshopUrl + '/getParameterTypes')
      .pipe(tap(parameterTypes => {
          console.log(parameterTypes);
          this.handleSuccess('Список типов параметров загружен');
          //    this.parameterTypes = parameterTypes;
        }),
        catchError(this.handleError(
          'Ошибка загузки типов параметров', [])));
  }

  saveOrUpdateParameterList(parameters: Parameter[]): Observable<Response>{

    return this.http.post<Response>(this.jsIshopUrl + `/saveOrUpdateParameterDtoList`,
      JSON.stringify(parameters), JsIshopService.httpOptions).
    pipe(
      tap((response: Response) => {
        // console.log(`Параметры успешно добавлены или обновлены`);
        this.handleSuccess('Параметры успешно добавлены или обновлены');

      }),
      catchError(this.handleError<Response>('Не удалось добавить/обновить параметры'))
    );
  }

}
