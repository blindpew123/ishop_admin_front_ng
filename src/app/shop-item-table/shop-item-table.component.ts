import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Category} from '../Data/category';
import {AdminService} from '../admin.service';
import {ShopItemDto} from '../Data/shop-item-dto';
import {TableOptionsComponent} from '../table-options/table-options.component';
import {MatDialog} from '@angular/material';
import {ShopItemsWithParamsListDto} from '../Data/shop-items-with-params-list-dto';
import {ShopItemSaveDto} from '../Data/shop-item-save-dto';

@Component({
  selector: 'app-shop-item-table',
  templateUrl: './shop-item-table.component.html',
  styleUrls: ['./shop-item-table.component.css']
})
export class ShopItemTableComponent implements OnInit, OnChanges {

  selectedRow: number = -1;
  baseForm: FormGroup;
  private _currentCategory: Category
  get rowsArray(): FormArray {
    return this.baseForm.get(this.rowsArrayName) as FormArray
  }
  set rowsArray(formArray: FormArray){
    this.baseForm.setControl(this.rowsArrayName, formArray);
  }
  // init
  rowsArrayName = 'shopItems';
  rowColumns: string[];
  rowVisibleColumns;
  private _isSubmited: boolean
  get isSubmited(): boolean{
    return this._isSubmited;
  }
  //-------------------------------
  //Child properties
  @Input()
  get currentCategory(): Category{
    return this._currentCategory;
  }
  set currentCategory(currentCategory: Category){
    this._currentCategory = currentCategory;
  }
  @Input()
  protected selectedTab: number;

  private _shopItemsDto: ShopItemsWithParamsListDto;
  get shopItemsDto(): ShopItemsWithParamsListDto{
    return this._shopItemsDto;
  }


  constructor(protected adminService: AdminService,
              private formBuilder: FormBuilder,
              public dialog: MatDialog) {
    this.initForm();
  }

  ngOnInit() {
  }

  processVisibleColumns(){
    this.rowVisibleColumns = {};
    this.getHeaderColumns().forEach(column=>{
      this.rowVisibleColumns[column] = true;
    })
    console.log(this.rowVisibleColumns);
    console.log(this.rowColumns);
  }

  private initForm(){
    this.baseForm = this.createFormGroup();
  }

  createFormGroup(): FormGroup {
    return new FormGroup({
      [this.rowsArrayName]: this.formBuilder.array([])
    });
  }

  addRow(){
    console.log("Valid ? " + this.baseForm.valid);
    if(!this.baseForm.valid || this.baseForm.dirty) {
      return
    }
    const emptyArrayForRow = new Array(this.shopItemsDto.shopItemsList.length);
    emptyArrayForRow[1] = this.currentCategory.idCategory;
    const newRow = this.fillRow(this.rowColumns,emptyArrayForRow);
    this.rowsArray.push(newRow);
    this.selectedRow = this.rowsArray.length-1;
  }

  deleteRow(){
    this.rowsArray.removeAt(this.selectedRow);
    // подписаться на удаление
    //  в подписке если успешно. удалить из формы
    //  удалить из массива и перезаполнить форму
  }

  _deleteRow(pos: number){
    this.rowsArray.removeAt(pos);
  }

  fillRows<T>(array: T[]) {
    this.initForm();
    array.forEach(objToRow => {
      this.rowsArray.push(
        this.fillRow(this.rowColumns, objToRow)
      );
    });
  }

  fillRow(rowColumns: string[], objToRow?: any, ): FormGroup{
    const formGroup = this.formBuilder.group({});
    if(objToRow instanceof Array) { // arrays proccessing by indexes
      for(let i = 0; i < this.rowColumns.length; i++) {
        formGroup.addControl(rowColumns[i], new FormControl(objToRow[i] || ''));
      }
    } else { // objects proccessing by predefined fields names
      this.rowColumns.forEach(column => {
        formGroup.addControl(column, new FormControl(objToRow[column] || ''));
      });
    }
    return formGroup
  }

  setActive(numRow: number){
      console.log("Valid ? " + this.baseForm.valid);
      if(!this.baseForm.valid) {
        return
      }
      if(this.selectedRow >=0 && numRow!=this.selectedRow){ // not first time
        this.onSubmit();
      }
      this.selectedRow = numRow;
  }

  // child methods -----------------
  openDialog(): void {
    const dialogRef = this.dialog.open(TableOptionsComponent, {
      width: '30%',
      data: this.rowVisibleColumns
    });
    dialogRef.afterClosed().subscribe(result => {
     // console.log(result);
    });
  }

  // Проверка изменения категории и страниы
    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    if(this.adminService.currentTab != 2){
      this.initForm();
      this.selectedRow = -1;
    }
    if(!!this.currentCategory) {
      let log: string[] = [];
      for (let propName in changes) {
        let changedProp = changes[propName];
        let to = JSON.stringify(changedProp.currentValue);
        if (changedProp.isFirstChange()) {
          log.push(`Initial value of ${propName} set to ${to}`);
        } else {
          let from = JSON.stringify(changedProp.previousValue);
          log.push(`${propName} changed from ${from} to ${to}`);
        }
      }
      console.log(log.join(', '));
      this.getShopItems(); // load
      this.setActive(-1);
    }
  }

  getShopItems(){
    // Загрузка
    this.adminService.getShopItems().subscribe(shopItemsDto=>{
      this._shopItemsDto = shopItemsDto;

      this.setupRowFields(); // field names ready
      if(this.adminService.isCategoryChanged) {
        this.processVisibleColumns();
      }
      this.fillRows(this.shopItemsDto.shopItemsList);
    });
  }

  setupRowFields(){
    const shopItemFields = (new ShopItemDto()).getFieldNames();
    this.rowColumns = [...shopItemFields];
    for(let i = shopItemFields.length; i < this.shopItemsDto.columnNames.length; i++){
      this.rowColumns[i] = 'field' + i;
    }
  }

  getHeaderColumns(): string[]{
    const head: string[] = [];
    for(let i = 2; i < this.shopItemsDto.columnNames.length; i++){
      head[i-2] = this.shopItemsDto.columnNames[i];
    }
    return head;
  }

  getAllowedForInputColumns(): string[] {
    const allowedForInputColumns: string[] = [];
    for(let i = 2; i < this.rowColumns.length; i++){
      allowedForInputColumns[i-2] = this.rowColumns[i];
    }
    console.log(allowedForInputColumns);
    return allowedForInputColumns;
  }

  isNumericTypeField(ordinal: number): boolean {
    return this.shopItemsDto.columnTypes[ordinal] ==='Число'
  }

  getFieldAlias(ordinal: number): string {
    return this.shopItemsDto.columnNames[ordinal];
  }

  getFieldValue(ordinal: number): any {
    this.shopItemsDto.shopItemsList[ordinal];
  }

  getAliasByName(name: string):string{
    for(let i = 0; i< this.rowColumns.length; i++){
      if(this.rowColumns[i] === name) {
        return this.shopItemsDto.columnNames[i];
      }
    }
    return '';
  }

  // process data ----------------

  onSubmit() {
    if (this.isSubmited) {
      return;
    }
    this._isSubmited = true;
    if (this.isRowChanged(this.selectedRow)) {
      console.warn(this.createShopItemSaveDto(this.rowsArray.value[this.selectedRow]));
      this.adminService
        .saveShopItem(this.createShopItemSaveDto(this.rowsArray.value[this.selectedRow]))
        .subscribe(response => {
          if (response === null) {
            this._isSubmited = false;
            this.updateComponentDtoData();
            this.selectedRow = -1;
            this.onCancel();
          }
        });
    }
  }

  onCancel(){
    this.selectedRow = -1;
    this.baseForm.reset();
    this.fillRows(this.shopItemsDto.shopItemsList);
  }


  isRowChanged(numRow: number): boolean{
    let isChanged = false;
    this.rowColumns.forEach((fieldName, i)=>{
      if(!this.shopItemsDto.shopItemsList[numRow] ||
        this.rowsArray.value[numRow][fieldName] !== this.shopItemsDto.shopItemsList[numRow][i]) {
        isChanged = true;
      }
    });
    return isChanged;
  }

  createShopItemSaveDto(objRow: any): ShopItemSaveDto{
    const shopItemSaveDto = new ShopItemSaveDto();
    shopItemSaveDto.shopItem = new ShopItemDto();
    shopItemSaveDto.paramValues = [];
    let counter = 0;
    const shopItemOwnFieldsLength = shopItemSaveDto.shopItem.getFieldNames().length;
    Object.getOwnPropertyNames(objRow).forEach(columnName=>{
      if(counter ++ < shopItemOwnFieldsLength) {
        shopItemSaveDto.shopItem[columnName] = objRow[columnName];
      } else {
        shopItemSaveDto.paramValues[counter - shopItemOwnFieldsLength - 1] = objRow[columnName];
      }
    })
    return shopItemSaveDto;
  }

  //Успешно измененные данные в базе заносятся в хранилище для ресета
  updateComponentDtoData(){
    this.rowsArray.value[this.selectedRow]
    if(!this.shopItemsDto.shopItemsList[this.selectedRow]){
      this.shopItemsDto.shopItemsList[this.selectedRow] = [];
    }
    this.rowColumns.forEach((fieldName, i)=>{
      this.shopItemsDto.shopItemsList[this.selectedRow][i] = this.rowsArray.value[this.selectedRow][fieldName]});
  }

}
