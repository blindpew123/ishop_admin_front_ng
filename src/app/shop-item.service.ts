import { Injectable } from '@angular/core';
import {JsIshopService} from './js-ishop-service';
import {HttpClient, HttpErrorResponse, HttpResponse, HttpResponseBase} from '../../node_modules/@angular/common/http';
import {MatBottomSheet} from '@angular/material';
import {Observable} from 'rxjs';
import {Category} from './Data/category';
import {catchError, tap} from 'rxjs/operators';
import {ShopItemsWithParamsListDto} from './Data/shop-items-with-params-list-dto';
import {ShopItemSaveDto} from './Data/shop-item-save-dto';

@Injectable({
  providedIn: 'root'
})
export class ShopItemService extends JsIshopService {

  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  getShopItems(category: Category): Observable<ShopItemsWithParamsListDto> {
    return this.http.get<ShopItemsWithParamsListDto>(this.jsIshopUrl + `/getShopItemsForCategory/${category.idCategory}`)
      .pipe( tap(ShopItemsWithParamsListDto => {
          console.log(ShopItemsWithParamsListDto);
          this.handleSuccess('Список товаров загружен');
        }),
        catchError(this.handleError('Ошибка загузки списка товаров', new ShopItemsWithParamsListDto())));
  }

  saveOrUpdateShopItem(shopItemSaveDto: ShopItemSaveDto): Observable<Response>{

    return this.http.post<Response>(this.jsIshopUrl + `/saveOrUpdateShopItem`,
      JSON.stringify(shopItemSaveDto), JsIshopService.httpOptions).
    pipe(
      tap((response: Response) => {
        // console.log(`Товар успешно добавлен или обновлен`);
        this.handleSuccess('Товар успешно добавлен или обновлен');

      }),
      catchError(this.handleError<Response>('Не удалось добавить/обновить товар'))
    );
  }

}
