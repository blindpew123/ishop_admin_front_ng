import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatBottomSheet} from '@angular/material';

import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Category} from './Data/category';
import {JsIshopService} from './js-ishop-service';
import {CategoryElementComponent} from './category-element/category-element.component';
import {CategoryPlaceComponent} from './category-place/category-place.component';
import {CategoryBase} from './category-base';
import {DeleteCategoryDto} from './Data/delete-category-dto';

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends JsIshopService{

  public _categories: Category[];
  get categories(): Category[]{
    return this._categories;
  }
  set categories(categories: Category[]){
    this._categories = categories;
  }


  private _categoryPlace: CategoryPlaceComponent;
  get categoryPlace(): CategoryPlaceComponent{
    return this._categoryPlace;
  }
  set categoryPlace(categoryPlace: CategoryPlaceComponent){
    this._categoryPlace = categoryPlace;
  }


  constructor(private http: HttpClient, protected bottomSheet: MatBottomSheet) {
    super(bottomSheet);
  }

  // оставить здесь
  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.jsIshopUrl + '/categoriesManagement')
      .pipe( tap(categories => {
          console.log(categories);
          this.handleSuccess('Список категорий загружен');
          this.categories = categories;
        }),
        catchError(this.handleError('Ошибка загузки списка категорий', [])));
  }
  // оставить здесь
  // TODO: JavaDoc
  // TODO: Отправлять DTO
  // TODO: Анализировать Ответ
  saveOrUpdateCategory(category: Category): Observable<Category>{
    return this.http.post<Category>(this.jsIshopUrl + `/saveOrUpdateCategory`,
            JSON.stringify(category), JsIshopService.httpOptions).
        pipe(
          tap((savedOrUpdatedCategory: Category) => {
            // console.log(`Категория с успешно добавлена или обновлена`);
            this.handleSuccess('Категория успешно добавлена или обновлена');
          }),
          catchError(this.handleError<Category>('Не удалось добавить/обновить категорию'))
        );
  }
  // оставить здесь
  // TODO: JavaDoc
  deleteCategory(deleteCategoryDto: DeleteCategoryDto): Observable<Response> {
      return this.http.post<Response>(this.jsIshopUrl + `/deleteCategory`,
      JSON.stringify(DeleteCategoryDto), JsIshopService.httpOptions).
      pipe(
        tap((response: Response) => {
      console.log(`Категория успешно удалена` + response.ok);
      this.handleSuccess('Категория успешно удалена' + response.ok);
    }),
    catchError(this.handleError<Response>('Не удалось удалить категорию', Response.error()))
    );
  }


}

