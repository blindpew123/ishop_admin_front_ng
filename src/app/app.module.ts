import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BottomSheetComponent } from './bottom-sheet/bottom-sheet.component';
import { MenuComponent } from './menu/menu.component';
import { CategoriesComponent } from './categories/categories.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {MaterialModule} from './material/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { CategoryElementComponent } from './category-element/category-element.component';
import { CategoryPlaceComponent } from './category-place/category-place.component';
import { ParameterFormComponent } from './parameter-form/parameter-form.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { ShopItemTableComponent } from './shop-item-table/shop-item-table.component';
import { TableOptionsComponent } from './table-options/table-options.component';
import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    BottomSheetComponent,
    MenuComponent,
    CategoriesComponent,
    PageNotFoundComponent,
    CategoryElementComponent,
    CategoryPlaceComponent,
    ParameterFormComponent,
    AdminPanelComponent,
    CategoryFormComponent,
    ShopItemTableComponent,
    TableOptionsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  entryComponents: [
    BottomSheetComponent,
    TableOptionsComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
