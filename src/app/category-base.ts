import {Category} from './Data/category';

export interface CategoryBase {
  toggleSelected(): void;
  getCategory(): Category;
  setCategory(category: Category);
}
