export class DeleteCategoryDto {
  categoryId: number;
  withChildren: boolean;
}
