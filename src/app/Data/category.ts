/**
 * Uses for transfer data to controller
 */

export class Category {
  idCategory: number;
  categoryName: string;
  parentId: number;
  categoryNumber: number;
}
