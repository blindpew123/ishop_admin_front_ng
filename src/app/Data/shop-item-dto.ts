export class ShopItemDto {
  idShopItem: number = 0;
  idCategory: number = 0;
  itemName: string = '';
  price: number = 0;
  weight: number = 0;
  volume: number = 0;
  qnty: number = 0;
  imageLink: string = '';

  public getFieldNames(): string[]{
    return Object.getOwnPropertyNames(this);
  }
}
