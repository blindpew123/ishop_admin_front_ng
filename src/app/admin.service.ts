import { Injectable } from '@angular/core';
import {CategoryService} from './category.service';
import {ParameterService} from './parameter.service';
import {Category} from './Data/category';
import {CategoryBase} from './category-base';
import {Observable} from 'rxjs';
import {Parameter} from './Data/parameter';
import {ParameterType} from './Data/parameter-type';
import {DeleteCategoryDto} from './Data/delete-category-dto';
import {ShopItemDto} from './Data/shop-item-dto';
import {ShopItemsWithParamsListDto} from './Data/shop-items-with-params-list-dto';
import {ShopItemService} from './shop-item.service';
import {ShopItemSaveDto} from './Data/shop-item-save-dto';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private _currentTab: number = 0;
  public get currentTab(): number{
    return this._currentTab;
  }
  public set currentTab(currentTab: number){
    this._currentTab = currentTab;
  }

  private _categoryChanged: boolean;

  get isCategoryChanged(): boolean {
      return this._categoryChanged;
  }

  private _topCategories: Category[];
  get topCategories(): Category[]{
    return this._topCategories;
  }
  set topCategories(topCategories: Category[]){
    this._topCategories = topCategories;
}

  private _element: CategoryBase; // selected Category Element
  get element(): CategoryBase {
    return this._element;
  }
  set element(categoryElement: CategoryBase){
    this._categoryChanged = !(this._element === categoryElement);
    this._element = categoryElement;
  }

  private categoryPalceholders: Category[][];

  // From Parameter Service
  private _paramterTypes: ParameterType[];
  get parameterTypes(): ParameterType[] {
    return this._paramterTypes;
  }
  set parameterTypes(parameterTypes: ParameterType[]) {
    this._paramterTypes = parameterTypes;
  }


  constructor(private categoryService: CategoryService,
              private parameterService: ParameterService,
              private shopItemService: ShopItemService) {
    this.categoryPalceholders = [];
    this.getCategories();
    this.getParameterTypes();
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(categories => this.topCategories = this.getTopLevel(categories));
  }
  //ParameterType request to Controller
  private getParameterTypes(){
    this.parameterService.getParameterTypes().subscribe(parameterTypes=>{
      this.parameterTypes = parameterTypes;
    });
  }

  //Creates new placeholder's category or gets it from cache.
  //TODO: watch can it be called with undefined parameters?
  getNewCategory(parent: number, ordinal: number): Category {
    if(!this.categoryPalceholders[+parent]){
      this.categoryPalceholders[+parent] = [];
    }
    if(!this.categoryPalceholders[+parent][+ordinal]){
      this.categoryPalceholders[+parent][+ordinal] = new Category();
      this.categoryPalceholders[+parent][+ordinal].categoryNumber = ordinal;
      this.categoryPalceholders[+parent][+ordinal].parentId = parent;

    }
    return this.categoryPalceholders[+parent][+ordinal];
  }

  // All Categories of top level (parent==null)
  getTopLevel(categories: Category[]):Category[]{
    const result: Category[] =[];
    let pos = 0;
    for (let i = 0; i < categories.length; i++){
      if(categories[i].parentId === 0){
        result[pos++] = categories[i];
      }
    }
    return result;
  }

  getChildren(idCategory: number): Category[]{
    const result: Category[] =[];
    let pos = 0;
    for (let i = 0; i < this.categoryService.categories.length; i++){
      if(this.categoryService.categories[i].parentId === idCategory){
        result[pos++] = this.categoryService.categories[i];
      }
    }
    return result;
  }

  saveOrUpdateCategory(category: Category): Observable<Category>{
    return this.categoryService.saveOrUpdateCategory(category);
  }

  getParametersForCurrentCategory(): Observable<Parameter[]> {
    if(!!this.element && this.element.getCategory().idCategory > 0) {
      return this.parameterService.getParametersForCategory(this.element.getCategory());
    }
  }

  deleteCategory(deleteCategoryDto: DeleteCategoryDto): Observable<Response>{
    return this.deleteCategory(deleteCategoryDto);
  }

  resetCurrentCategoryElement(){
    this.element = null;
  }

  //Parameters -----------------------------------------------------------

  saveOrUpdateParameterList(parameters: Parameter[]): Observable<Response> {
    return this.parameterService.saveOrUpdateParameterList(parameters);
  }

  // ShopItems ------------------------------------------------------------

  getShopItems(){
    return this.shopItemService.getShopItems(this.element.getCategory());
  }

  saveShopItem(shopItemSaveDto: ShopItemSaveDto): Observable<Response>{
    return this.shopItemService.saveOrUpdateShopItem(shopItemSaveDto);
  }




}
