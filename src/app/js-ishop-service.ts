import {HttpHeaders} from '@angular/common/http';
import {MatBottomSheet} from '@angular/material';
import {BottomSheetComponent} from './bottom-sheet/bottom-sheet.component';
import {Observable, of} from 'rxjs';

export class JsIshopService {
  protected static httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  protected jsIshopUrl = 'http://localhost:8080/iShop/admin';  // URL to web api !! NO '/' at the end of string

  constructor(protected bottomSheet: MatBottomSheet) {}

  protected openBottomSheet(data: any): void {
    this.bottomSheet.open(BottomSheetComponent, data);
  }

  protected handleSuccess(serviceSuccessMessage: string, result?: any): void {
    this.openBottomSheet({data: {
        info: serviceSuccessMessage,
        obj: !!result ? result : {}
      }});
  }

  // ToDo: check how will be handled error messages from controllers
  protected handleError<T> (serviceErrMessage = 'Ошибка', result?: T) {
    return (error: any): Observable<T> => {
      this.openBottomSheet({data: {
          error: {message: serviceErrMessage + ` (${error.status})`}
        }});

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  protected getRightWord(num: number): string {
    let result: string;
    const tmpStr: string = '' + num;
    const val: number = +tmpStr.charAt(tmpStr.length - 1);
    switch (val) {
      case 1:
        result = ' запись';
        break;
      case 2:
      case 3:
      case 4:
        result = ' записи';
        break;
      default:
        result = ' записей';
    }
    return result;
  }
}
