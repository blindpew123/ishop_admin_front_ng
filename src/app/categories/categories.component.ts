import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Category} from '../Data/category';
import {CategoryService} from '../category.service';
import {NgForm} from '@angular/forms';
import {ParameterService} from '../parameter.service';
import {MatTabChangeEvent} from '@angular/material';
import {ParameterFormComponent} from '../parameter-form/parameter-form.component';
import {AdminService} from '../admin.service';

@Component({
  selector: 'app-category',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})



export class CategoriesComponent implements OnInit {

  @ViewChild(ParameterFormComponent)
  private _parameterFormComponent: ParameterFormComponent;
  get parameterFormComponent(): ParameterFormComponent{
    return this._parameterFormComponent;
  }


  private _topCategories: Category[];
  get topCategories(): Category[]{
    return this._topCategories;
  }
  @Input()
  set topCategories(topCategories: Category[]){
    this._topCategories = topCategories;
  }



  constructor(protected adminService: AdminService) { }

  ngOnInit() {}







}

