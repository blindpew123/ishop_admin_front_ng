import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {CategoriesComponent} from './categories/categories.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';

const appRoutes: Routes = [
 /* {
    path: 'bookDetail/:id', // TODO: исправить
    component: BookDetailComponent
  },*/
  {
    path: 'categoriesManagement',
    component: AdminPanelComponent,
    runGuardsAndResolvers: 'always',
    data: { title: 'Управление'}
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload'})
  ],
  declarations: []
})
export class AppRoutingModule {
  public static getRoutings(): Routes {
    return appRoutes;
  }
}
